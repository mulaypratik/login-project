package LoginForm;

import javax.swing.*;

import java.awt.event.*;
import java.awt.*;
import java.sql.*;

public class LoginForm extends JFrame implements ActionListener
{
	public JTextField t1;
	public JPasswordField t2;
	public JLabel l1, l2;
	public JButton b1, b2;
	public String str1, str2, nm, nm2;
	
	public LoginForm()
	{
		setTitle("Shopping Mall Management System");
		setSize(400, 200);
		t1=new JTextField(20); 
		t2=new JPasswordField(20);
		l1=new JLabel("       Username:");
		l2=new JLabel("       Password:");
		b1=new JButton("Login");
		b2=new JButton("Cancel");
		l1.setLabelFor(t1);
		l2.setLabelFor(t2);
		setLayout(new GridLayout(3,2,5,5));
		add(l1);
		add(t1);
		add(l2);
		add(t2);
		add(b1);
		add(b2);
		b1.addActionListener(this);
		b2.addActionListener(this);
		//setTitle("Welcome to Secure Chat!");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		
	}

	public void compare() throws Exception
	{
		str1=t1.getText();
		str2=t2.getText();
		try
		{
	      Class.forName( "sun.jdbc.odbc.JdbcOdbcDriver" ) ;

	      Connection conn = DriverManager.getConnection( "jdbc:odbc:Login" );
	      Statement stmt = conn.createStatement();
	      Statement stmt2=conn.createStatement();
      	  ResultSet rs=stmt2.executeQuery("SELECT * FROM Details WHERE u_name= '"+str1+"' AND psw='"+str2+"'" );
	      if(rs.next())
	      {
	         JOptionPane.showMessageDialog(this, "Authentication Successful.");
	         t1.setText("");
	         t2.setText("");
	      }
	      else
	         {
	    	  JOptionPane.showMessageDialog(this, "Authentication Fail.");
	    	  t1.setText("");
		      t2.setText("");
	         }
	      rs.close();
	      stmt.close();
	      stmt2.close();
	      conn.close();
	     }
	  catch( SQLException se )
	     {
	      System.out.println( "SQL Exception:");
	      se.printStackTrace();
	     }

	}
	
	public static void main(String[] args) 
	{
			new LoginForm();
	}
	
	public void actionPerformed(ActionEvent arg0)
	{
		Object o=arg0.getSource();
		if(o==b1)
		{
			if((t1.getText().length()==0) || (t2.getText().length()==0))
			{
		    	  JOptionPane.showMessageDialog(this, "Please enter Username & Password.");
			}
			else
			{
			try {
				
				compare();
				
			} catch (Exception e) {
						

e.printStackTrace();
			}
			}
			
		}
		else
		{
			System.exit(0);
		}
			
	    
		
	}

}
